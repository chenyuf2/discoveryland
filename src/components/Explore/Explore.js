import React, {Component} from 'react';
import "./Explore.css";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import Fade from 'react-reveal/Fade';

export default class Explore extends Component {
    componentDidMount = ()=>{
        window.addEventListener('scroll', this.handleScroll);
    }
    handleScroll = () => {
        console.log(2);
        var value = window.scrollY * 0.25;
        document.getElementById("holding").style.transform = "rotate(" + value+"deg)";
    }
    render() {
        const CustomRightArrow = ({ onClick, ...rest }) => {
            const {
              onMove,
              carouselState: { currentSlide, deviceType } 
            } = rest;
            // onMove means if dragging or swiping in progress.
            return <button>Wocao</button>;
          };
        const responsive = {
            superLargeDesktop: {
              // the naming can be any, depends on you.
              breakpoint: { max: 4000, min: 3000 },
              items: 5
            },
            desktop: {
              breakpoint: { max: 3000, min: 1024 },
              items: 3
            },
            tablet: {
              breakpoint: { max: 1024, min: 464 },
              items: 2
            },
            mobile: {
              breakpoint: { max: 464, min: 0 },
              items: 1
            }
          };
        return (
            <div className="explore-container">
                <div className="explore-inner">
                    <div className="explore-content">
                        <div className="slider-none">
                            <div className="explore-text">
                                <div className="explore-text-container">
                                    <h1 className="explore-h1">
                                        <Fade bottom cascade>
                                        <div className="explore-h1-font">Explore</div>
                                        </Fade>
                                        <span>
                                        <Fade bottom cascade>
                                            <div>our worlds</div>
                                        </Fade>
                                        </span>
                                    </h1>
                                    <div>
                                    <Fade bottom cascade>

                                        <p className="sub-p">
                                            From majestic mountainscapes to beautiful coastlines... breathtaking worlds to uncover and experience.
                                        </p>
                                        </Fade>
                                    </div>
                                </div>
                                <div className="slider-part">
                                <img id="holding" className="hold-img" src="https://discoverylandco.com/static/hold-n-drag.svg" alt="Hold &amp; Drag" draggable="false"></img>
                                <Carousel 
                                    ref={(el) => (this.Carousel = el)} 
                                 afterChange={() => {
                                     var index = parseFloat(this.Carousel.state.currentSlide);
                                     var percent = 100 * (index / 20);
                                    //  console.log(index);
                                    //  console.log(percent);
                                     document.getElementById("progress").style.width = '' + percent + '%';
                                     
                                    //  this.Carousel.state.currentSlide
                                    }}
                                 swipeable={true}
                                 draggable={true}
                                responsive={responsive}
                                removeArrowOnDeviceType={["tablet", "mobile"]}
                                dotListClass="custom-dot-list-style"
                                itemClass=""
                                keyBoardControl={true}
                                customTransition="transform 600ms ease-in-out"
                                transitionDuration={600}
                                style={{whiteSpace: 'nowrap!important', display: 'flex!important'}}
                                >
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/365/chileno-carousel_190704_100748_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Chileno Bay</h1>
                                <p class="slide-text">Cabo San Lucas, Mexico</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/6669/Troubadour_Andy-Carlson_Golf_October-2019_DJI_0172-copycrop_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Troubadour</h1>
                                <p class="slide-text">Nashville, Tennessee</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/695/Bakers_Marina_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Baker's Bay</h1>
                                <p class="slide-text">Great Guana Cay, Bahamas</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/211/Summit-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">The Summit</h1>
                                <p class="slide-text">Las Vegas, Nevada</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/188/driftwood-1_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Driftwood</h1>
                                <p class="slide-text">Austin, Texas</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/203/ElDorado-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">El Dorado</h1>
                                <p class="slide-text">Los Cabos, Mexico</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/6796/IMG_0911_397186feb8bd51bb37bc24fc2b9b500f.jpeg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Mākena</h1>
                                <p class="slide-text">Maui, Hawaii</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/6654/Silo_Andy-Carlson_golf_October-2019_DJI_0020-copycrop_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Silo Ridge</h1>
                                <p class="slide-text">Amenia, New York</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/212/Yellowstone-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Yellowstone Club</h1>
                                <p class="slide-text">Big Sky, Montanao</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/206/Grande-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Playa Grande</h1>
                                <p class="slide-text">Rio San Juan, Dominican Republic</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/6492/LM-Coco-Point-Barbuda-0266_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Barbuda Ocean Club</h1>
                                <p class="slide-text">Barbuda, West Indies</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/370/Madison2-Carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Madison</h1>
                                <p class="slide-text">La Quinta, California</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style"draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/479/Kukio-map1_397186feb8bd51bb37bc24fc2b9b500f.JPG"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Kūki'o</h1>
                                <p class="slide-text">Kohala, Hawaii</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/82/Dune_deck_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Dune Deck</h1>
                                <p class="slide-text">Westhampton Beach, NY</p>
                                </div>
                                </div>
                                <div className="img-container"> 
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/617/clubhouse-copy_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Hideaway</h1>
                                <p class="slide-text">La Quinta, California</p>
                                </div>
                                </div>
                                <div className="img-container"> 
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/205/GozzerRanch-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Gozzer Ranch</h1>
                                <p class="slide-text">Coeur d’Alene, Idaho</p>
                                </div>
                                </div>
                                <div className="img-container"> 
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/717/MOUNTAINTOP_CAROUSEL_190715_144237_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Mountaintop</h1>
                                <p class="slide-text">Cashiers, North Carolina</p>
                                </div>
                                </div>
                                <div className="img-container"> 
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/372/mirabel-carousel_190704_104754_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Mirabel</h1>
                                <p class="slide-text">Scottsdale, Arizona</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/208/ironhorse-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Iron Horse</h1>
                                <p class="slide-text">Whitefish, Montana</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/213/Vaquero-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Vaquero</h1>
                                <p class="slide-text">Westlake, Texas</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/244/Estancia-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">Estancia</h1>
                                <p class="slide-text">Scottsdale, Arizona</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img className="img-style" draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/249/CordeValle-carousel_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">CordeValle</h1>
                                <p class="slide-text">Silicon Valley, California</p>
                                </div>
                                </div>
                                <div className="img-container">
                                <img style={{width: '30vw', height: '85%', objectFit: 'cover' }} draggable="false" src="https://dxaurk9yhilm4.cloudfront.net/images/6894/IMG_00162-copy_397186feb8bd51bb37bc24fc2b9b500f.jpg"></img>
                                <div className="img-content-container">
                                <h1 class="slide-title">CostaTerra</h1>
                                <p class="slide-text">Comporta, Portugal</p>
                                </div>
                                </div>
                                </Carousel>
                                <div className="progress-bar">
                                    <div id="progress" className="actual-bar"></div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
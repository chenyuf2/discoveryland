import React, {Component} from 'react';
import "./Expand.css";
export default class Expand extends Component {
    helperHide = (id, index) => {
        document.getElementById(id).style.opacity = "1";
        console.log( document.getElementById("video-detail-block" + index).style.opacity);
        document.getElementById("video-detail-block" + index).style.opacity = 1;
        for (let i = 1; i <= 3; i++) {
            document.getElementById("content" + i).style.animation = "showup 0.3s linear forwards";
            document.getElementById("figure" + i).style.animation = "shiftLeft 0.9s linear 0.2s forwards";
            document.getElementById("detail-img" + i).style.animation = "shiftRight 0.9s linear 0.2s forwards";
        }

        setTimeout(function(){
            for (var i = 1; i <= 3; i++) {
                document.getElementById("figure" + i).style.opacity = "0";
            }
        }, 1100)
    }
    helperShow = (id, index) => {
        console.log(3);
        for (let i = 1; i <= 3; i++) {
            document.getElementById("figure" + i).style.opacity = "1";
            console.log(document.getElementById("video-detail-block" + i).style.opacity);
            if (document.getElementById("video-detail-block" + i).style.opacity === "1") {
                console.log(i);
                // document.getElementById("video-detail-block" + i).style.opacity = '0';

                document.getElementById("video-detail-block" + i).style.animation = 'important 0.3s linear 0s forwards';
            }
        }
        
        for (var i = 1; i <= 3; i++) {
            document.getElementById("content" + i).style.animation = "showoff 0.3s linear forwards";
            document.getElementById("figure" + i).style.animation = "shiftLeftBack 0.9s linear forwards";
            document.getElementById("detail-img" + i).style.animation = "shiftRightBack 0.9s linear forwards";
        }
        setTimeout(function(){
            for (var i = 1; i <= 3; i++) {
                document.getElementById("detailVideo" + i).style.opacity = "0";
                document.getElementById("video-detail-block" + i).style.opacity = "0";
                document.getElementById("video-detail-block" + i).style.animation = "";
            }
        }, 900)
    }
    openVideo1 = (e) => {
        e.preventDefault();
        if (document.getElementById("figure1").style.opacity !== '0') {
            this.helperHide("detailVideo1", 1);
        } else {
            this.helperShow("detailVideo1", 1);
        }
    }
    openVideo2 = (e) => {
        e.preventDefault();
        if (document.getElementById("figure2").style.opacity !== '0') {
            this.helperHide("detailVideo2", 2);
        } else {
            this.helperShow("detailVideo2", 2);
        }
    }
    openVideo3 = (e) => {
        e.preventDefault();
        if (document.getElementById("figure3").style.opacity !== '0') {
            this.helperHide("detailVideo3", 3);
        } else {
            this.helperShow("detailVideo3", 3);
        }
    }
    render() {
        return (
            <div className="expand-container">
                <div className="video-container-box">
                    <div></div>
                    <div className="video-common" id="detailVideo1">
                        <video autoPlay id="vid1" src={require("../../foot1.mp4")} muted loop></video>
                    </div>
                    <div className="video-common" id="detailVideo2" >
                        <video id="vid2" src={require("../../foot2.mp4")} autoPlay  muted loop></video>
                    </div>
                    <div className="video-common" id="detailVideo3" >
                        <video id="vid3" src={require("../../foot3.mp4")} autoPlay  muted loop></video>
                    </div>
                </div>
                <div className="video-detail">
                    <div className="video-detail-container" id="video-detail-block1">
                        <div className="content-row" >
                            <article className="article-style">
                                <h1 className="big-title">
                                    Experiences
                                </h1>
                                <p className="small-title-container">
                                    <div className="small-title">
                                        <div className="mr-1 small-common">Experience</div>
                                        <div className="mr-1 small-common">the</div>
                                        <div className="mr-1 small-common">ultimate</div>
                                        <div className="mr-1 small-common">round</div>
                                        <div className="mr-1 small-common">of</div>
                                        <div className="mr-1 small-common">golf</div>
                                        <div className="mr-1 small-common">on</div>
                                        <div className="mr-1 small-common">championship</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">courses</div>
                                        <div className="mr-1 small-common">designed</div>
                                        <div className="mr-1 small-common">by</div>
                                        <div className="mr-1 small-common">the </div>
                                        <div className="mr-1 small-common">world's</div>
                                        <div className="mr-1 small-common">most</div>
                                        <div className="mr-1 small-common">highly</div>
                                        <div className="mr-1 small-common">acclaimed</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">architects</div>
                                        <div className="mr-1 small-common">or</div>
                                        <div className="mr-1 small-common">take</div>
                                        <div className="mr-1 small-common">a</div>
                                        <div className="mr-1 small-common">thrilling</div>
                                        <div className="mr-1 small-common">adventure</div>
                                        <div className="mr-1 small-common">with</div>
                                        <div className="mr-1 small-common">limitless</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">activities</div>
                                        <div className="mr-1 small-common">for</div>
                                        <div className="mr-1 small-common">everyone</div>
                                        <div className="mr-1 small-common">to</div>
                                        <div className="mr-1 small-common">enjoy.</div>
                                    </div>
                                </p>
                                <a className="explore-btn">
                                    <div style={{display: 'flex'}}>Explore</div></a>
                            </article>
                        </div>
                        </div>
                        <div className="video-detail-container" id="video-detail-block2">
                        <div className="content-row" >
                            <article className="article-style">
                                <h1 className="big-title">
                                    Lifestyle
                                </h1>
                                <p className="small-title-container">
                                    <div className="small-title">
                                        <div className="mr-1 small-common">Relish</div>
                                        <div className="mr-1 small-common">the</div>
                                        <div className="mr-1 small-common">very</div>
                                        <div className="mr-1 small-common">best</div>
                                        <div className="mr-1 small-common">times</div>
                                        <div className="mr-1 small-common">of</div>
                                        <div className="mr-1 small-common">your</div>
                                        <div className="mr-1 small-common">life</div>
                                        <div className="mr-1 small-common">with</div>
                                        <div className="mr-1 small-common">the</div>
                                        <div className="mr-1 small-common">ones</div>
                                        <div className="mr-1 small-common">you</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">love</div>
                                        <div className="mr-1 small-common">in</div>
                                        <div className="mr-1 small-common">your</div>
                                        <div className="mr-1 small-common">bespoke </div>
                                        <div className="mr-1 small-common">communities</div>
                                        <div className="mr-1 small-common">that</div>
                                        <div className="mr-1 small-common">cater</div>
                                        <div className="mr-1 small-common">to</div>
                                        <div className="mr-1 small-common">a</div>
                                        <div className="mr-1 small-common">family</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">orientated</div>
                                        <div className="mr-1 small-common">lifestyle</div>
                                        <div className="mr-1 small-common">through</div>
                                        <div className="mr-1 small-common">unparalleled</div>
                                        <div className="mr-1 small-common">amenities</div>
                                        <div className="mr-1 small-common">and</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">inspired</div>
                                        <div className="mr-1 small-common">experiences.</div>
                                    </div>
                                </p>
                                <a className="explore-btn">
                                    <div style={{display: 'flex'}}>Explore</div></a>
                            </article>
                        </div>
                        </div>
                        <div className="video-detail-container" id="video-detail-block3">
                        <div className="content-row" >
                            <article className="article-style">
                                <h1 className="big-title">
                                    Wellness
                                </h1>
                                <p className="small-title-container">
                                    <div className="small-title">
                                        <div className="mr-1 small-common">Relax</div>
                                        <div className="mr-1 small-common">and</div>
                                        <div className="mr-1 small-common">refresh</div>
                                        <div className="mr-1 small-common">your</div>
                                        <div className="mr-1 small-common">body</div>
                                        <div className="mr-1 small-common">and</div>
                                        <div className="mr-1 small-common">your</div>
                                        <div className="mr-1 small-common">mind</div>
                                        <div className="mr-1 small-common">in</div>
                                        <div className="mr-1 small-common">exquisite</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">surroundings.</div>
                                        <div className="mr-1 small-common">Escape</div>
                                        <div className="mr-1 small-common">to</div>
                                        <div className="mr-1 small-common">our </div>
                                        <div className="mr-1 small-common">stunning's</div>
                                        <div className="mr-1 small-common">pools</div>
                                        <div className="mr-1 small-common">unwind</div>
                                        <div className="mr-1 small-common">in</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">relaxing</div>
                                        <div className="mr-1 small-common">spas</div>
                                        <div className="mr-1 small-common">and</div>
                                        <div className="mr-1 small-common">explore</div>
                                        <div className="mr-1 small-common">innovative</div>
                                        <div className="mr-1 small-common">international</div>
                                    </div>
                                    <div className="small-title">
                                        <div className="mr-1 small-common">culinary</div>
                                        <div className="mr-1 small-common">offerings.</div>
                                    </div>
                                </p>
                                <a className="explore-btn">
                                    <div style={{display: 'flex'}}>Explore</div></a>
                            </article>
                        </div>
                        </div>
                    
                </div>
                <div className="three-blocks">
                    <div className="img-outer"  onClick={this.openVideo1.bind(this)}>
                        <div className="img-inner">
                            <figure className="figure-style-cool" id="figure1">
                                {/* <picture> */}
                                {/* <source srcset="https://dxaurk9yhilm4.cloudfront.net/images/728/Expereinces_slider_190716_095517_464875650e0665072829219c2109f3b9.jpg" media="(max-width: 700px)"></source> */}
                                <img id="detail-img1" className="img-temp"  src="https://dxaurk9yhilm4.cloudfront.net/images/728/Expereinces_slider_190716_095517_e36797c82dbcb9dea0800f713424f022.jpg"></img>
                                {/* </picture> */}
                            </figure>
                            <div className="figure-content" id="content1">
                                <h4 className="figure-title">Experiences</h4>
                                <div className="circle-group">
                                    <div className="circle-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                    <div className="circle-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="img-outer" onClick={this.openVideo2.bind(this)}>
                        <div className="img-inner">
                            <figure className="figure-style-cool" id="figure2" >
                            {/* <picture> */}
                            {/* <source srcset="https://dxaurk9yhilm4.cloudfront.net/images/729/lifestyle_slider_190716_095537_0b2516e730d44c336347c45fc67effd5.jpg" media="(max-width: 700px)"></source> */}
                            <img id="detail-img2" className="img-temp" src="https://dxaurk9yhilm4.cloudfront.net/images/729/lifestyle_slider_190716_095537_fc98236fcd7afd38ca0fd181cf61bcb6.jpg" style={{transform: "matrix(1,0,0,1,0,0)"}}></img>
                            {/* </picture> */}
                            </figure>
                            <div className="figure-content" id="content2">
                                <h4 className="figure-title">Lifestyle</h4>
                                <div className="circle-group">
                                    <div className="circle-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                    <div className="circle-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="img-outer" onClick={this.openVideo3.bind(this)}>
                        <div className="img-inner">
                            <figure className="figure-style-cool" id="figure3" >
                            {/* <picture> */}
                            {/* <source srcset="https://dxaurk9yhilm4.cloudfront.net/images/730/wellness_slider_190716_095551_7a63ac04cea98219d28754079427d015.jpg" media="(max-width: 700px)"></source> */}
                            <img id="detail-img3" className="img-temp" src="https://dxaurk9yhilm4.cloudfront.net/images/730/wellness_slider_190716_095551_90f8ce634cc6ad7a15d605007f03ee5c.jpg" style={{transform: "matrix(1,0,0,1,0,0)"}}></img>
                            {/* </picture> */}
                            </figure>
                            <div className="figure-content" id="content3">
                                <h4 className="figure-title">Wellness</h4>
                                <div className="circle-group">
                                    <div className="circle-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                    <div className="circle-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.06 20.91" fill="white"><path d="M0 11.1h25.86L16.97 20c0 .04.91.92.91.91l10-10c0-.01.01-.01.02-.02l.04-.04c.01-.01.02-.02.02-.03.01-.01.02-.03.03-.05.01-.01.01-.03.02-.04 0-.01.01-.02.01-.03v-.02c.01-.01.01-.03.01-.04 0-.02.01-.03.01-.05 0-.01 0-.03.01-.04 0-.02 0-.03.01-.05v-.05-.05c0-.02 0-.03-.01-.05 0-.01 0-.03-.01-.04 0-.02-.01-.03-.01-.05 0-.01-.01-.03-.01-.04 0-.01 0-.01-.01-.02 0-.01-.01-.02-.02-.03-.01-.01-.01-.03-.02-.04-.01-.02-.02-.03-.03-.05-.01-.01-.02-.02-.02-.03-.01-.01-.02-.03-.04-.04.01 0 0-.01 0-.01l-10-10c0-.01-.91.91-.91.91l8.89 8.9H0v1.29z"></path></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
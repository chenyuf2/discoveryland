import React, {Component} from 'react';
import Nav from "../Nav/Nav";
import "./Home.css";
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';
import Welcome from '../Welcome/Welcome';
import Explore from '../Explore/Explore';
import Footer from '../Footer/Footer';
import Expand from '../Expand/Expand';
import Earth from '../Earth/Earth';
export default class Home extends Component {
    constructor() {
        super();
        this.state = {
            slideIndex:0,
        }
    }
    slideLeft = (e) => {
        e.preventDefault();
        if (this.state.slideIndex == 1) {
            document.getElementById("video1").style.opacity = "0";
            document.getElementById("video4").style.opacity = "1";
            document.getElementById("v4").load();
            this.setState({slideIndex: 4});
        } else if (this.state.slideIndex == 2) {
            document.getElementById("video2").style.opacity = "0";
            document.getElementById("video1").style.opacity = "1";
            document.getElementById("v1").load();
            this.setState({slideIndex: 1});
        } else if (this.state.slideIndex == 3) {
            document.getElementById("video3").style.opacity = "0";
            document.getElementById("video2").style.opacity = "1";
            document.getElementById("v2").load();
            this.setState({slideIndex: 2});
        } else if (this.state.slideIndex == 4) {
            document.getElementById("video4").style.opacity = "0";
            document.getElementById("video3").style.opacity = "1";
            document.getElementById("v3").load();
            this.setState({slideIndex: 3});
        }
    }
    setNone = (id) => {
        document.getElementById(id).style.opacity = '0';
    }
    slideRight = (e) => {
        e.preventDefault();
        let temp = this.state.slideIndex;
        this.setState({slideIndex: (temp + 1) % 4}, () => {
            for (var i = 3; i >= 0; i--) {
                if (i !== temp && i !== this.state.slideIndex) {
                    document.getElementById("video" + i).style.opacity= "0";
                }
                document.getElementById("video" + i).style.animation= "";
                document.getElementById("v" + i).style.animation = "";
            }
            document.getElementById("video" + this.state.slideIndex).style.opacity = "1";
            document.getElementById("v" + this.state.slideIndex).load();
            document.getElementById("video" + this.state.slideIndex).style.animation = "shiftLeft 1.1s ease-in";
            document.getElementById("v" + this.state.slideIndex).style.animation= "videoShiftLeft 1.1s ease-in";
        });
    }
    render() {
        const AutoplaySlider = withAutoplay(AwesomeSlider);
        return (
            <section>
                <Nav></Nav>
                <div className="outer">
                    <div className="contains">
                        <h1 className="main-title">
                            <div className="title-container">
                                <div className="first-line">
                                    <div className="main-alpha" id="title1">D</div>
                                    <div className="main-alpha" id="title2">i</div>
                                    <div className="main-alpha" id="title3">s</div>
                                    <div className="main-alpha" id="title4">c</div>
                                    <div className="main-alpha" id="title5">o</div>
                                    <div className="main-alpha" id="title6">v</div>
                                    <div className="main-alpha" id="title7">e</div>
                                    <div className="main-alpha" id="title8">r</div>
                                </div>
                                <div className="second-line">
                                            <div className="mr-4" style={{display: 'flex'}}>
                                                <div className="main-alpha" id="sub1">y</div>
                                                <div className="main-alpha" id="sub2">o</div>
                                                <div className="main-alpha" id="sub3">u</div>
                                                <div className="main-alpha" id="sub4">r</div>
                                            </div>
                                            <div style={{display: 'flex'}}>
                                                <div className="main-alpha" id="sub5">w</div>
                                                <div className="main-alpha" id="sub6">o</div>
                                                <div className="main-alpha" id="sub7">r</div>
                                                <div className="main-alpha" id="sub8">l</div>
                                                <div className="main-alpha" id="sub9">d</div>
                                            </div>
                                </div>
                            </div>
                        </h1>
                        <AutoplaySlider animation="foldOutAnimation"
                        play={true}
                        cancelOnInteraction={false} // should stop playing on user interaction
                        interval={10000}>
                            <div className ="video-container" id="video0" >
                                <video id="v0" loop muted playsInline autoPlay src={require("../../video1.mp4")}></video>
                                    <div className="sub-container">
                                        <div className="sub-title">
                                            <div  className="mr-1">
                                                Indulge
                                            </div >
                                            <div  className="mr-1">
                                                your 
                                            </div>
                                            <div  className="mr-1">
                                                adventurous
                                            </div>
                                            <div>
                                                spirit
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className ="video-container" id="video1" style={{transform: 'matrix(1,0,0,1,0,0)'}}>
                                <video id="v1" loop muted playsInline autoPlay src={require("../../video2.mp4")} style={{transform: 'matrix(1,0,0,1,0,0)'}}></video>
                                    <div className="sub-container">
                                        <div className="sub-title">
                                            <div className="mr-1">
                                                Explore 
                                            </div>
                                            <div  className="mr-1">
                                                astounding 
                                            </div>
                                            <div  className="mr-1">
                                                mountain 
                                            </div>
                                            <div>
                                                scenery
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className ="video-container" id="video2" style={{transform: 'matrix(1,0,0,1,0,0)'}}>
                                <video id="v2" loop muted playsInline autoPlay src={require("../../video3.mp4")} style={{transform: 'matrix(1,0,0,1,0,0)'}}></video>
                                <div className="sub-container">
                                        <div className="sub-title">
                                            <div  className="mr-1">
                                                Journey 
                                            </div>
                                            <div  className="mr-1">
                                                across 
                                            </div>
                                            <div  className="mr-1">
                                                the 
                                            </div>
                                            <div>
                                                desert
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className ="video-container" id="video3" style={{transform: 'matrix(1,0,0,1,0,0)'}}>
                                <video id="v3" loop muted playsInline autoPlay src={require("../../video4.mp4")} style={{transform: 'matrix(1,0,0,1,0,0)'}}></video>
                                <div className="sub-container">
                                    <div className="sub-title">
                                        <div  className="mr-1">
                                            Roam 
                                        </div>
                                        <div  className="mr-1">
                                            pristine 
                                        </div>
                                        <div>
                                            coastline
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </AutoplaySlider>
                        <div className="scroll-down">
                            <span className="scroll-down-text">Scroll down</span>
                        </div>
                    </div>
                </div>
                <div style={{height: 'calc(100vh - 15vh)', position: 'relative'}}></div>
                <Welcome></Welcome>
                <Explore></Explore>
                <Earth></Earth>
                <Expand></Expand>
                <Footer></Footer>
            </section>
        )
    }
}
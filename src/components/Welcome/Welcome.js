import React, {Component} from 'react';
import "./Welcome.css";

import Fade from 'react-reveal/Fade';
export default class Welcome extends Component {
    render() {
        return (
            <div className="welcome-container">
                <div className="inner-welcome">
                    <div className="background-layout"></div>
                    <div className="introduction-container">
                        <div className="introduction-subcontainer">
                            <div className="grid-container">
                                <div className="grid-text">
                                   
                                    <h3 className="first-text-container">
                                        <Fade bottom cascade>
                                        <div className="welcome-first">Welcome to</div>
                                        </Fade>
                                        <span>
                                            <Fade bottom cascade>
                                            <div className="welcome-second">
                                                Our world
                                            </div>
                                            </Fade>
                                        </span>
                                    </h3>
                                   
                                    <div className="detail-text-block">
                                        
                                        <p> 
                                        <Fade bottom cascade>
                                            <div className="common-line">Discover the unique atmosphere of our private</div>
                                        </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">residential club communities. Once experienced. Never</div>
                                            </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">forgotten. This is classic, comfortable, modern living in </div>
                                            </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">nature’s most spectacular settings worldwide. </div>
                                            </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">Reassuringly exclusive. Generously welcoming. Find </div>
                                            </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">your unique world – a place where families love to be; </div>
                                            </Fade>
                                        <Fade bottom cascade>
                                            <div className="common-line">creating unforgettable moments, together. </div>
                                        </Fade>
                                        </p>
                                    </div>
                                </div>
                                <div className="first-img-container">
                                    <figure className="first-figure">
                                        <img src="https://dxaurk9yhilm4.cloudfront.net/images/275/Home_76fe09b415dcada4f46ba4114338562c.jpg"></img>
                                    </figure>
                                </div>
                                <div className="second-img-container">
                                    <figure className="second-figure">
                                        <img src="https://dxaurk9yhilm4.cloudfront.net/images/27/DriftwoodDevelopment_JessicaJohnson_March2019_52_df8586bb4c14d18f77324f7452f392cd.jpg"></img>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
import React from 'react';
import logo, { ReactComponent } from './logo.svg';
import './App.css';
import Home from './components/Home/Home';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter basename="/discoveryland">
        <Switch>
          <Route path="/" exact>
            <Home></Home>
          </Route>
        </Switch>
      </BrowserRouter>
    )
  }
}


